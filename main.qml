
import QtQuick 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0
import QtMultimedia 5.6

Item {
	id: self
	width: 1400; height: 920

	property var scenes: null

	property int currentSceneIndex: -1
	onCurrentSceneIndexChanged: {
		if (currentSceneIndex !== -1) loadScene(currentSceneIndex);
	}
	property var scene: self.currentSceneIndex === -1 ? null : self.scenes[self.currentSceneIndex]

	property var imageData: null
	property int paletteTime: 0
	property var currentRequest: null

	Component.onCompleted: {
		var r = new XMLHttpRequest();
		r.onreadystatechange = function() {
			if (r.readyState === XMLHttpRequest.DONE) {
				if (Math.floor(r.status/100) === 2 && r.responseText) {
					eval(r.responseText);
					self.scenes = scenes;
				}
				currentSceneIndex = 2;
			}
		};
		r.open('GET', 'http://www.effectgames.com/demos/canvascycle/scenes.js');
		r.send();
	}

	Loader {
		active: self.scene && self.scene.sound
		sourceComponent: MediaPlayer {
			id: player
			autoPlay: true
			loops: MediaPlayer.Infinite
			source: 'http://www.effectgames.com/demos/canvascycle/audio/' + scene.sound + '.mp3'
			volume: (self.scene.maxVolume !== undefined ? self.scene.maxVolume : 1) * 0.2 * volumeSlider.value
		}
	}

	Timer {
		running: self.imageData
		repeat: true
		interval: 16
		onTriggered: {
			self.paletteTime += interval;
			paletteCanvas.requestPaint();
		}
	}

	function findCycle(index) {
		for (var i = 0; i < self.imageData.cycles.length; i++) {
			var c = self.imageData.cycles[i];
			if (c.low < 0 || c.high > 255 || c.rate <= 0) continue;
			if (index >= c.low && index <= c.high) return c;
		}
	}

	function calcCycleOffset(cycle, time) {
		var range = cycle.high - cycle.low + 1;
		var interval = 1000.0 / cycle.rate * 200.0 * range;
		var absoffset = (time % interval) / interval * range;
		var diroffset = cycle.reverse === 2 ? absoffset : range - absoffset;
		return diroffset;
	}

	function calcPaletteColor(index, time) {
		var cycle = self.findCycle(index);
		if (!cycle) return self.imageData.colors[index];

		var offset = calcCycleOffset(cycle, time);
		var range = cycle.high - cycle.low + 1;

		var c1 = self.imageData.colors[(index - cycle.low + Math.round(offset)) % range + cycle.low];
		if (!blend.checked) return c1;

		var df = offset - Math.round(offset);
		var f = 1.0 - Math.abs(df);
		var rf = 1.0 - f;
		var c2 = self.imageData.colors[(index - cycle.low + Math.round(offset) + (df < 0 ? -1 : 1)) % range + cycle.low];
		return [c1[0]*f + c2[0]*rf, c1[1]*f + c2[1]*rf, c1[2]*f + c2[2]*rf];
	}

	property Canvas paletteCanvas: Canvas {
		width: 16; height: 16
		onPaint: {
			if (!self.imageData) return;
			var p = getContext('2d');
			for (var i = 0; i < self.imageData.colors.length; i++) {
				var c = self.calcPaletteColor(i, paletteTime);
				p.fillStyle = Qt.rgba(c[0] / 255, c[1] / 255, c[2] / 255, 1.0);
				p.fillRect(i % 16, Math.floor(i / 16), 1, 1);
			}
		}
	}

	property Canvas imageCanvas: Canvas {
		onPaint: {
			if (!self.imageData) return;
			var p = getContext('2d');
			p.globalCompositeOperation = 'copy';
			for (var x = 0; x < width; x++) {
				for (var y = 0; y < height; y++) {
					p.fillStyle = Qt.rgba(
						self.imageData.pixels[x*2 + 0 + y * self.imageData.width] / 255,
						self.imageData.pixels[x*2 + 1 + y * self.imageData.width] / 255,
						0, 1
					);
					p.fillRect(x, y, 1, 1);
				}
			}
		}
	}

	function loadScene(sceneIndex) {
		if (self.currentRequest) {
			self.currentRequest.abort();
			self.currentRequest = null;
		}

		var CanvasCycle = {
			processImage: function (d) {
				self.imageData = d;
				self.imageCanvas.canvasSize = Qt.size(d.width / 2, d.height);
				self.imageCanvas.width = self.imageCanvas.canvasSize.width;
				self.imageCanvas.height = self.imageCanvas.canvasSize.height;
				self.paletteCanvas.requestPaint();
				self.imageCanvas.requestPaint();
			}
		};

		var r = new XMLHttpRequest();
		r.onreadystatechange = function() {
			if (r.readyState === XMLHttpRequest.DONE) {
				if (Math.floor(r.status/100) === 2 && r.responseText) {
					eval(r.responseText);
				}
				self.currentRequest = null;
			}
		};
		r.open('GET', 'http://www.effectgames.com/demos/canvascycle/image.php?file=' + self.scenes[sceneIndex].name);
		r.send();

		self.currentRequest = r;
	}

	ColumnLayout {
		spacing: 5
		anchors { fill: parent; rightMargin: 260 }

		RowLayout {
			height: 128

			ShaderEffectSource {
				width: 128; height: parent.height
				sourceItem: self.imageCanvas
				smooth: false
			}

			ShaderEffectSource {
				width: 128; height: parent.height
				textureSize: Qt.size(16, 16)
				smooth: false
				sourceItem: self.paletteCanvas
			}

			Loader {
				id: image
				active: self.imageData
				width: 128; height: parent.height

				sourceComponent: ShaderEffectSource {
					textureSize: Qt.size(self.imageData.width, self.imageData.height)
					sourceItem: ShaderEffect {
						width: self.imageData.width
						height: self.imageData.height
						readonly property var image: ShaderEffectSource { sourceItem: imageCanvas; smooth: false }
						readonly property var palette: ShaderEffectSource { sourceItem: paletteCanvas; smooth: false }
						readonly property real imageWidth: self.imageData.width

						fragmentShader: '
							varying highp vec2 qt_TexCoord0;
							uniform sampler2D image;
							uniform sampler2D palette;
							uniform highp float imageWidth;

							void main() {
								lowp vec4 c = texture2D(image, qt_TexCoord0);
								mediump float index = (0.5 + c[int(mod(qt_TexCoord0.x * imageWidth, 2.0))] * 255.0) / 256.0 * 16.0;
								gl_FragColor = texture2D(palette, vec2(fract(index), (floor(index) + 0.5) / 16.0));
							}
						'
					}
				}
			}
		}

		Loader {
			Layout.fillHeight: true
			Layout.preferredWidth: !self.imageData ? 0 : height / self.imageData.height * self.imageData.width
			Layout.alignment: Qt.AlignCenter
			active: self.imageData
			sourceComponent: ShaderEffect {
				property var source: image.item
				layer { effect: GaussianBlur { radius: 4; samples: 16; deviation: 3 } enabled: linear.checked }
			}
		}
	}

	ColumnLayout {
		anchors { right: parent.right; top: parent.top; bottom: parent.bottom }
		width: 240

		Button {
			text: 'Next'
			enabled: self.scenes
			onClicked: self.currentSceneIndex = (self.currentSceneIndex + 1) % self.scenes.length;
		}

		CheckBox { id: blend; text: "Blend"; checked: true }
		CheckBox { id: linear; text: "Smooth"; checked: true }
		Slider { id: volumeSlider; value: 0.1 }

		ListView {
			Layout.fillWidth: true
			Layout.fillHeight: true
			model: self.scenes ? self.scenes.length : 0
			currentIndex: self.currentSceneIndex
			clip: true
			delegate: Item {
				id: delegate
				width: ListView.view.width; height: 28
				Text { text: self.scenes[model.index].title; font.bold: delegate.ListView.isCurrentItem }
				MouseArea { anchors.fill: parent; onClicked: self.currentSceneIndex = model.index; }
			}
		}
	}

	Item {
		anchors.fill: parent
		visible: (self.currentRequest && self.scene) || !self.scenes

		Text {
			id: loadingLabel
			color: 'white'
			font.pointSize: 30
			text: !self.scenes ? 'Loading scenes' : 'Loading: ' + (self.scene ? self.scene.title : '')
			anchors.centerIn: parent
			z: 2
		}

		Rectangle {
			anchors.fill: loadingLabel
			anchors.margins: -30
			radius: height/2
			color: '#90000000'
			z: 1
		}
	}
}
